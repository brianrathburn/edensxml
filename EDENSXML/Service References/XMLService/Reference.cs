﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EDENSXML.XMLService {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="XMLService.IEdensXMLFeed")]
    public interface IEdensXMLFeed {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IEdensXMLFeed/UploadXMLasString", ReplyAction="http://tempuri.org/IEdensXMLFeed/UploadXMLasStringResponse")]
        EDENSXML.XMLService.UploadXMLasStringResponse UploadXMLasString(EDENSXML.XMLService.UploadXMLasStringRequest request);
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="UploadXMLasString", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class UploadXMLasStringRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="http://tempuri.org/", Order=0)]
        public string edensXmlAsString;
        
        public UploadXMLasStringRequest() {
        }
        
        public UploadXMLasStringRequest(string edensXmlAsString) {
            this.edensXmlAsString = edensXmlAsString;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.MessageContractAttribute(WrapperName="UploadXMLasStringResponse", WrapperNamespace="http://tempuri.org/", IsWrapped=true)]
    public partial class UploadXMLasStringResponse {
        
        public UploadXMLasStringResponse() {
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IEdensXMLFeedChannel : EDENSXML.XMLService.IEdensXMLFeed, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class EdensXMLFeedClient : System.ServiceModel.ClientBase<EDENSXML.XMLService.IEdensXMLFeed>, EDENSXML.XMLService.IEdensXMLFeed {
        
        public EdensXMLFeedClient() {
        }
        
        public EdensXMLFeedClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public EdensXMLFeedClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public EdensXMLFeedClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public EdensXMLFeedClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public EDENSXML.XMLService.UploadXMLasStringResponse UploadXMLasString(EDENSXML.XMLService.UploadXMLasStringRequest request) {
            return base.Channel.UploadXMLasString(request);
        }
    }
}
