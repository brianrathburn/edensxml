USE MSTR_ODS

SELECT	'1.0' '@Version'
	  , GETDATE() Created
	  ,( SELECT	RIGHT(LTRIM(RTRIM(MGK.CCCO)), 4) '@ID'
					  , CASE WHEN LTRIM(RTRIM(MGBU.MCDL03)) <> '' THEN LTRIM(RTRIM(MGBU.MCDL03))
							 ELSE LTRIM(RTRIM(MGK.CCNAME))
						END Name
		  , ( SELECT	RIGHT(LTRIM(RTRIM(K.CCCO)), 4) '@ID'
					  , CASE WHEN LTRIM(RTRIM(B.MCDL03)) <> '' THEN LTRIM(RTRIM(B.MCDL03))
							 ELSE LTRIM(RTRIM(K.CCNAME))
						END Name
					  , LTRIM(RTRIM(Region.DRDL01)) Region
					  , LTRIM(RTRIM(Status.DRDL01)) PropertyStatus
					  , LTRIM(RTRIM(MIN(J.ALADD1))) Address
					  , LTRIM(RTRIM(MIN(J.ALCTY1))) City
					  , LTRIM(RTRIM(MIN(J.ALADDS))) State
					  , LTRIM(RTRIM(MIN(J.ALADDZ))) Zip
					  , SUM(CAST(A.SqFt AS INT)) TotalSqFt
					  , ( SELECT	C.CBSA
								  , F.cLat
								  , F.cLong
								  , ( SELECT	RING_DEFN '@Miles'
											  , ( SELECT	TOTPOP_CY POP
														  , TOTHH_CY HH
														  , AVGHINC_CY AHI
												FOR
												  XML PATH('')
													, TYPE
												)
									  FROM		SDE.SDEPlanner.DEMORINGS_EVW E
									  WHERE		RING_DEFN IN ( 1, 3, 5, 10, 15 )
												AND CenterID = C.CenterID
									FOR
									  XML PATH('Radius')
										, ROOT('Demographics')
										, TYPE
									)
						  FROM		SDE.SDEPlanner.CENTERS_EVW C
									JOIN SDE.SDEPlanner.CENTERS_POINTS_EVW F
										ON C.CenterID = F.CenterID
										   AND RIGHT(LTRIM(RTRIM(K.CCCO)), 4) = C.CenterID
						FOR
						  XML PATH('GISData')
							, TYPE
						)
					  , ( SELECT	CASE WHEN LEFT(UNIT, 1) = '0' THEN RIGHT(LTRIM(RTRIM(UNIT)), LEN(LTRIM(RTRIM(UNIT))) - 1)
										 WHEN LEFT(UNIT, 2) = '00' THEN RIGHT(LTRIM(RTRIM(UNIT)), LEN(LTRIM(RTRIM(UNIT))) - 2)
										 ELSE LTRIM(RTRIM(UNIT))
									END '@Unit'
								  , Q.ID
								  , Q.Name
								  , Q.StartDate
								  , Q.SqFt
						  FROM		( SELECT	LTRIM(RTRIM(G.NWUNIT)) Unit
											  , CAST(NEAN8 AS INT) ID
											  , UPPER(CASE WHEN CHARINDEX('#', LTRIM(RTRIM(NEDL01))) > 2
														   THEN LTRIM(RTRIM(LEFT(LTRIM(RTRIM(NEDL01)), CHARINDEX('#', NEDL01) - 1)))
														   ELSE LTRIM(RTRIM(NEDL01))
													  END) Name
											  , CAST(master.dbo.fn_JDETODATE(NEEFTB) AS DATE) StartDate
											  , CAST(SUM(G.NWPMU1 / 100) AS INT) SqFt
											  , ROW_NUMBER() OVER ( PARTITION BY G.NWMCU, G.NWUNIT ORDER BY NEEFTB ) Row_N
									  FROM		MSTR_ODS.dbo.st_F15017 C
											  , MSTR_ODS.dbo.st_F1501B D
											  , MSTR_ODS.dbo.st_F1507 E
											  , MSTR_ODS.dbo.st_F1514 G
									  WHERE		LEFT(LTRIM(C.NWMCU), 4) = CAST(K.CCCO AS INT)
												AND C.NWMCU = E.NHMCU
												AND G.NWUNIT = E.NHUNIT
												AND C.NWDOCO = D.NEDOCO
												AND C.NWLSVR = D.NELSVR
												AND C.NWMCU = G.NWMCU
												AND C.NWUNIT = G.NWUNIT
												AND LTRIM(RTRIM(NWARTY)) = CASE NHUTTY
																			 WHEN 'LSABL' THEN 'GLA'
																			 ELSE 'OTH'
																		   END
												AND C.NWLSVR = ( SELECT	MAX(NWLSVR)
																 FROM	MSTR_ODS.dbo.st_F15017 B
																 WHERE	C.NWDOCO = B.NWDOCO
															   )
												AND E.NHUTTY NOT IN ( 'DISAB', 'OTHER', 'OEA', 'UNALL' )
												AND G.NWEFTE = '0'
												AND C.NWMODT = '0'
												AND ( NEEFTE > master.dbo.fn_DATETOJDE(GETDATE())
													  OR NEEFTE = '0'
													)
												AND D.NELSST NOT IN ( 'Z', 'R', 'F' )
												AND D.NELSET <> 'AN'
									  GROUP BY	G.NWMCU
											  , G.NWUNIT
											  , D.NEAN8
											  , D.NEEFTB
											  , D.NELSST
											  , D.NEDL01
											  , E.NHUST
									  UNION
									  SELECT	LTRIM(RTRIM(G.NWUNIT)) Unit
											  , NULL ID
											  , 'AVAILABLE' NAME
											  , NULL StartDate
											  , CAST(SUM(G.NWPMU1 / 100) AS INT) SqFt
											  , 1 
									  FROM		MSTR_ODS.dbo.st_F1507 E
											  , MSTR_ODS.dbo.st_F1514 G
									  WHERE		LEFT(LTRIM(NWMCU), 4) = CAST(K.CCCO AS INT)
												AND NWMCU = E.NHMCU
												AND NWUNIT = E.NHUNIT
												AND NWMCU = G.NWMCU
												AND NWUNIT = G.NWUNIT
												AND LTRIM(RTRIM(NWARTY)) = CASE NHUTTY
																			 WHEN 'LSABL' THEN 'GLA'
																			 ELSE 'OTH'
																		   END
												AND E.NHUTTY NOT IN ( 'DISAB', 'OTHER', 'OEA', 'UNALL' )
												AND G.NWEFTE = '0'
												AND E.NHUST = 'V'
									  GROUP BY	NWUNIT
											  , E.NHUST
									) Q
						  WHERE		Q.Row_N = 1
						  ORDER BY	Q.Unit
						FOR
						  XML PATH('Tenant')
							, ROOT('Tenants')
							, TYPE
						)
					  , ( SELECT	CASE CategoryCode
									  WHEN 'MCRP06' THEN 'Region'
									  WHEN 'MCRP16' THEN 'PropertyManager'
									  WHEN 'MCRP17' THEN 'LeasingRep'
									  WHEN 'MCRP21' THEN 'Developer'
									END '@Type'
								  , LTRIM(RTRIM(DRSPHD)) ID
								  , CASE LTRIM(RTRIM(DRSPHD))
									  WHEN '' THEN ''
									  ELSE LTRIM(RTRIM(DRDL01))
									END Name
								  , CASE LTRIM(RTRIM(DRSPHD))
									  WHEN '' THEN ''
									  ELSE LTRIM(RTRIM(DRDL02))
									END Phone
								  , CASE LTRIM(RTRIM(DRSPHD))
									  WHEN '' THEN ''
									  ELSE LTRIM(RTRIM(EAEMAL))
									END Email
						  FROM		( SELECT	pivotA.CategoryCode
											  , pivotA.Value
									  FROM		( SELECT	CAST(LTRIM(Aa.MCRP16) AS VARCHAR(3)) MCRP16
														  , CAST(LTRIM(Aa.MCRP17) AS VARCHAR(3)) MCRP17
														  , CAST(LTRIM(Aa.MCRP21) AS VARCHAR(3)) MCRP21
												  FROM		MSTR_ODS.dbo.st_F0006 Aa
												  WHERE		Aa.MCCO = K.CCCO
															AND LTRIM(Aa.MCRP08) NOT IN ( '', '300', '400', '600', '990', '995' )
															AND LTRIM(Aa.MCSTYL) IN ( 'PR' )
												  GROUP BY	Aa.MCRP16
														  , Aa.MCRP17
														  , Aa.MCRP21
												) A UNPIVOT ( Value FOR CategoryCode IN ( MCRP16, MCRP17, MCRP21 ) ) pivotA
									) A
									LEFT JOIN ( SELECT	B.DRKY
													  , B.DRRT
													  , B.DRDL01
													  , B.DRDL02
													  , B.DRSPHD
												FROM	MSTR_ODS.dbo.st_F0005 B
												WHERE	LTRIM(B.DRSY) = '00'
														AND LTRIM(B.DRRT) IN ( '16', '17', '21' )
											  ) B
										ON LTRIM(B.DRKY) = A.value
										   AND LTRIM(B.DRRT) = RIGHT(A.CategoryCode, 2)
									LEFT JOIN dbo.st_F01151 C
										ON B.DRSPHD = C.EAAN8
						FOR
						  XML PATH('Contact')
							, ROOT('Contacts')
							, TYPE
						)
			  FROM		MSTR_ODS.dbo.st_F0006 B
						JOIN MSTR_ODS.dbo.st_F0010 K
							ON B.MCCO = K.CCCO
						JOIN MSTR_ODS.dbo.st_F0005 Status
							ON LTRIM(B.MCRP08) = LTRIM(Status.DRKY)
							   AND LTRIM(Status.DRSY) = '00'
							   AND LTRIM(Status.DRRT) = '08'
						JOIN MSTR_ODS.dbo.st_F0005 Region
							ON LTRIM(B.MCRP06) = LTRIM(Region.DRKY)
							   AND LTRIM(Region.DRSY) = '00'
							   AND LTRIM(Region.DRRT) = '06'
						JOIN MSTR_ODS.dbo.st_F0116 J
							ON B.MCAN8 = J.ALAN8
						LEFT OUTER JOIN ( SELECT	SUM(NWPMU1 / 100) SqFt
												  , LEFT(LTRIM(NWMCU), 4) NWMCU
										  FROM		MSTR_ODS.dbo.st_F1514
										  WHERE		NWARTY = 'GLA'
													AND NWARLL = 'F'
													AND NWEFTE = '0'
										  GROUP BY	LEFT(LTRIM(NWMCU), 4)
										) A
							ON RIGHT(K.CCCO, 4) = A.NWMCU
			  WHERE		LTRIM(B.MCRP08) NOT IN ( '', '300', '400', '600', '990', '995' )
						AND LTRIM(B.MCSTYL) = ( 'PR' )
						AND B.MCRP41 = ''
						AND b.mcrp42 = ''
						AND K.CCCO = MGK.CCCO
			  GROUP BY	K.CCCO
					  , K.CCNAME
					  , B.MCDL03
					  , Status.DRDL01
					  , Region.DRDL01
					  , J.ALADD1
					  , J.ALCTY1
					  , J.ALADDS
					  , J.ALADDZ
					  , A.SqFt
			FOR
			  XML PATH('Property')
				, ROOT('Properties')
				, TYPE
			)
			FROM		MSTR_ODS.dbo.st_F0006 MGBU
						JOIN MSTR_ODS.dbo.st_F0010 MGK
							ON MGBU.MCCO = MGK.CCCO
						JOIN MSTR_ODS.dbo.st_F0005 Status
							ON LTRIM(MGBU.MCRP08) = LTRIM(Status.DRKY)
							   AND LTRIM(Status.DRSY) = '00'
							   AND LTRIM(Status.DRRT) = '08'
						JOIN MSTR_ODS.dbo.st_F0005 Region
							ON LTRIM(MGBU.MCRP06) = LTRIM(Region.DRKY)
							   AND LTRIM(Region.DRSY) = '00'
							   AND LTRIM(Region.DRRT) = '06'
						JOIN MSTR_ODS.dbo.st_F0116 J
							ON MGBU.MCAN8 = J.ALAN8
			WHERE		LTRIM(MGBU.MCRP08) NOT IN ( '', '300', '400', '600', '990', '995' )
						AND LTRIM(MGBU.MCSTYL) = 'PR'
						AND MGBU.MCRP41 = ''
						AND MGBU.mcrp42 = ''
						AND MGBU.mcco != '02356'
			GROUP BY	MGK.CCCO
					  , MGK.CCNAME
					  , MGBU.MCDL03
			FOR
			  XML PATH('Group')
				, ROOT('Groups')
				, TYPE
			)
FOR		XML	PATH('EDENSData')
		  , TYPE        
