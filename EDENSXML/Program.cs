﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace EDENSXML
{
    public class Program
    {
        private static string finalXML;

        public static void Main(string[] args)
        {
            EDENSData finalData;
            XmlSerializer xs = new XmlSerializer(typeof(EDENSData));

            string fileName = "./sql_NonGrouped.sql";
            string xmlString = new GetData().GetXML(fileName);
            EDENSData nonGrouped = (EDENSData)xs.Deserialize(new StringReader(xmlString));

            fileName = "./sql_Grouped.sql";
            xmlString = new GetData().GetXML(fileName);
            EDENSData grouped = (EDENSData)xs.Deserialize(new StringReader(xmlString));

            finalData = nonGrouped;
            finalData.Groups.AddRange(grouped.Groups);

            using (EncodedStringWriter sw = new EncodedStringWriter(Encoding.UTF8))
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = System.Text.Encoding.UTF8;

                using (XmlWriter xw = XmlWriter.Create(sw, settings))
                {
                    xs.Serialize(xw, finalData);
                    finalXML = sw.ToString();
                    if (args.Count() == 1 && string.Equals(args[0], "sftp", StringComparison.CurrentCultureIgnoreCase))
                    {
                        UploadXMLviaSFTP uploadViasFTP = new UploadXMLviaSFTP();
                        uploadViasFTP.PerformUpload(finalXML);
                    }
                    else
                    {
                        using (UploadXML uploadXML = new UploadXML())
                        {
                            string message = uploadXML.PerformUploadAction(finalXML);
                        }
                    }
                }
            }
        }
    }
    public class EncodedStringWriter : StringWriter
    {
        public override Encoding Encoding { get; }

        public EncodedStringWriter(Encoding encoding)
        {
            Encoding = encoding;
        }
    }
}