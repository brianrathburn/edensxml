﻿using EDENSXML.XMLService;
using System;
using System.Configuration;
using System.ServiceModel.Security;

namespace EDENSXML
{
    public sealed class UploadXML : IDisposable
    {
        private EdensXMLFeedClient xmlClient;

        public UploadXML()
        {
            Console.WriteLine("\n.Opening Web Service");

            var userName = ConfigurationManager.AppSettings["XmlFeedUsername"];
            if (string.IsNullOrEmpty(userName))
            {
                throw new Exception("XmlFeedUsername app setting not configured in App.Config");
            }
            var password = ConfigurationManager.AppSettings["XmlFeedPassword"];
            if (string.IsNullOrEmpty(password))
            {
                throw new Exception("XmlFeedPassword app setting not configured in App.Config");
            }

            xmlClient = new EdensXMLFeedClient();

            xmlClient.ClientCredentials.UserName.UserName = userName;
            xmlClient.ClientCredentials.UserName.Password = password;
            xmlClient.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode =
                X509CertificateValidationMode.None;

            xmlClient.Open();
        }

        public string PerformUploadAction(string xmlString)
        {
            Console.WriteLine("..Uploading XML");

            UploadXMLasStringRequest upReq = new UploadXMLasStringRequest
            {
                edensXmlAsString = xmlString
            };

            UploadXMLasStringResponse upResp = xmlClient.UploadXMLasString(upReq);

            return string.Empty;
        }

        public void Dispose()
        {
            Console.WriteLine("...Upload process complete");
            if (xmlClient != null)
            {
                xmlClient.Close();
            }
        }
    }
}