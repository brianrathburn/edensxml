﻿using System;
using System.Data.SqlClient;
using System.IO;

namespace EDENSXML
{
    public class GetData
    {
        private string xmlString = string.Empty;
        private string sqlString = string.Empty;

        public string GetXML(string fileName)
        {
            Console.WriteLine("\n.Creating connection to SQL Server");

            using (FileStream fileStream = new FileStream(fileName, FileMode.Open))
            {
                byte[] byteBuffer = new byte[(int)fileStream.Length - 1];
                fileStream.Read(byteBuffer, 0, (int)fileStream.Length - 1);

                sqlString = System.Text.Encoding.ASCII.GetString(byteBuffer);
            }

            string connString = System.Configuration.ConfigurationManager.ConnectionStrings["MSTR_ODS"].ConnectionString;

            using (SqlConnection sqlconn = new SqlConnection(connString))
            {
                sqlconn.Open();

                using (SqlCommand sqlComm = new SqlCommand(sqlString, sqlconn))
                {
                    Console.WriteLine("..Generating XML");
                    xmlString = (string)sqlComm.ExecuteScalar();
                }
            }

            Console.WriteLine("...XML generation complete");

            return xmlString;
        }
    }
}