﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDENSXML
{
    public static class Extensions
    {
        public static int SafeConvertToInt(this string value)
        {
            int retVal = 0;
            int.TryParse(value, out retVal);
            return retVal;
        }
        public static string GetContainingDirectory(this string value)
        {
            if (value.Last() == '*')
            {
                return value.Remove(value.Length - 2, 2);
            }
            else if (value.Last() == '\\')
            {
                return value.Remove(value.Length - 1, 1);
            }
            else
            {
                return value;
            }
        }
    }
}
