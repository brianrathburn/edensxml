﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using WinSCP;

namespace EDENSXML
{
    public class UploadXMLviaSFTP
    {
        private static int _hostPort;
        private static string _hostName;
        private static string _userName;
        private static string _password;
        private static string _sshHostKey;
        private static string _tempDirectory;
        private static string _destinationPath;
        private static string _sessionLogPath;
        private static string _debugLogPath;

        public UploadXMLviaSFTP()
        {
            InitializeSettings();

        }
        public bool PerformUpload(string xmlString)
        {
            Console.WriteLine("sFTP upload started");
            var tempFile = WriteTempFile(xmlString);
            var success = false;
            SessionOptions sessionOptions = new SessionOptions
            {
                Protocol = Protocol.Sftp,
                HostName = _hostName,
                UserName = _userName,
                Password = _password,
                PortNumber = _hostPort,
                SshHostKeyFingerprint = _sshHostKey
            };

            using (Session session = new Session())
            {
                if (!string.IsNullOrEmpty(_sessionLogPath))
                {
                    session.SessionLogPath = _sessionLogPath;
                }
                if (!string.IsNullOrEmpty(_debugLogPath))
                {
                    session.DebugLogPath = _debugLogPath;
                }
                session.Open(sessionOptions);

                TransferOptions transferOptions = new TransferOptions();
                transferOptions.TransferMode = TransferMode.Binary;
                transferOptions.FilePermissions = null;
                transferOptions.PreserveTimestamp = false;
                transferOptions.ResumeSupport.State = TransferResumeSupportState.Off;

                TransferOperationResult transferResult = session.PutFiles(_tempDirectory, _destinationPath, true, transferOptions);
                transferResult.Check();                
                success = transferResult.IsSuccess;
                if (success)
                {
                    Console.WriteLine("sFTP upload succeeded");
                }
                else
                {
                    Console.WriteLine("sFTP upload failed, please check the log file for details");
                }
            }
            return success;
        }

        private string WriteTempFile(string xmlString)
        {
            var outputFile = Path.Combine(_tempDirectory.GetContainingDirectory(), "EdensData.xml");
            File.WriteAllText(outputFile, xmlString);
            return outputFile;
        }

        private void InitializeSettings()
        {
            _hostName = ConfigurationManager.AppSettings["HostName"];
            if (string.IsNullOrEmpty(_hostName))
            {
                throw new Exception("HostName app setting not configured in App.Config");
            }
            _hostPort = ConfigurationManager.AppSettings["HostPort"].SafeConvertToInt();
            if (_hostPort == 0)
            {
                _hostPort = 22;
            }
            _userName = ConfigurationManager.AppSettings["UserName"];
            if (string.IsNullOrEmpty(_userName))
            {
                throw new Exception("UserName app setting not configured in App.Config");
            }
            _password = ConfigurationManager.AppSettings["Password"];
            if (string.IsNullOrEmpty(_password))
            {
                throw new Exception("Password app setting not configured in App.Config");
            }
            _sshHostKey = ConfigurationManager.AppSettings["SshHostKey"];
            if (string.IsNullOrEmpty(_sshHostKey))
            {
                throw new Exception("SshHostKey app setting not configured in App.Config");
            }
            _tempDirectory = ConfigurationManager.AppSettings["TempDirectory"];
            if (string.IsNullOrEmpty(_tempDirectory))
            {
                throw new Exception("TempDirectory app setting not configured in App.Config");
            }
            _destinationPath = ConfigurationManager.AppSettings["DestinationPath"];
            if (string.IsNullOrEmpty(_destinationPath))
            {
                throw new Exception("DestinationPath app setting not configured in App.Config");
            }
            _sessionLogPath = ConfigurationManager.AppSettings["SessionLogPath"];
            _debugLogPath = ConfigurationManager.AppSettings["DebugLogPath"];
        }
    }
}
