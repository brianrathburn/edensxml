USE MSTR_ODS

SELECT	'1.0' '@Version'
	  , GETDATE() Created
	  , ( SELECT	LTRIM(RTRIM(MGBU.MCRP41)) '@ID'
				  , LTRIM(RTRIM(MktgGroup.DRDL01)) Name
	  , ( SELECT	CASE WHEN LTRIM(RTRIM(B.MCRP41)) IN ('2141', '2142', '2143', '2148', '2351', '2370', '2431', '3008')
							THEN LTRIM(RTRIM(B.MCMCU))
						ELSE LTRIM(RTRIM(B.MCRP41))
					END '@ID'
				  , CASE WHEN LTRIM(RTRIM(B.MCRP41)) IN ('2141', '2142', '2143', '2148', '2351', '2370', '2431', '3008')
							THEN CASE WHEN LTRIM(RTRIM(B.MCDL03)) <> '' 
										THEN LTRIM(RTRIM(B.MCDL03)) 
									ELSE LTRIM(RTRIM(B.MCDL01)) END	
						ELSE CASE WHEN LTRIM(RTRIM(B.MCDL03)) <> '' 
									THEN LTRIM(RTRIM(B.MCDL03)) 
								ELSE LTRIM(RTRIM(MktgGroup.DRDL01)) END	
					END Name
				  , LTRIM(RTRIM(Region.DRDL01)) Region
				  , LTRIM(RTRIM(Status.DRDL01)) PropertyStatus
				  , LTRIM(RTRIM(MIN(J.ALADD1))) Address
				  , LTRIM(RTRIM(MIN(J.ALCTY1))) City
				  , LTRIM(RTRIM(MIN(J.ALADDS))) State
				  , LTRIM(RTRIM(MIN(J.ALADDZ))) Zip
				  , SUM(CAST(A.SqFt AS INT)) TotalSqFt
				  , ( SELECT	C.CBSA
							  , F.cLat
							  , F.cLong
							  , ( SELECT	RING_DEFN '@Miles'
										  , ( SELECT	TOTPOP_CY POP
													  , TOTHH_CY HH
													  , AVGHINC_CY AHI
											FOR
											  XML PATH('')
												, TYPE
											)
								  FROM		SDE.SDEPlanner.DEMORINGS_EVW E
								  WHERE		RING_DEFN IN ( 1, 3, 5, 10, 15 )
											AND CenterID = C.CenterID
								FOR
								  XML PATH('Radius')
									, ROOT('Demographics')
									, TYPE
								)
					  FROM		SDE.SDEPlanner.CENTERS_EVW C
								JOIN SDE.SDEPlanner.CENTERS_POINTS_EVW F
									ON C.CenterID = F.CenterID
									   AND LTRIM(RTRIM(B.MCRP41)) = C.CenterID
					FOR
					  XML PATH('GISData')
						, TYPE
					)
				  , ( SELECT	CASE WHEN LEFT(UNIT, 1) = '0' THEN RIGHT(LTRIM(RTRIM(UNIT)), LEN(LTRIM(RTRIM(UNIT))) - 1)
									 WHEN LEFT(UNIT, 2) = '00' THEN RIGHT(LTRIM(RTRIM(UNIT)), LEN(LTRIM(RTRIM(UNIT))) - 2)
									 ELSE LTRIM(RTRIM(UNIT))
								END '@Unit'
							  , Q.ID
							  , Q.Name
							  , Q.StartDate
							  , Q.SqFt
					  FROM		( SELECT	LTRIM(RTRIM(G.NWUNIT)) Unit
										  , CAST(NEAN8 AS INT) ID
										  , UPPER(CASE WHEN CHARINDEX('#', LTRIM(RTRIM(NEDL01))) > 2
													   THEN LTRIM(RTRIM(LEFT(LTRIM(RTRIM(NEDL01)), CHARINDEX('#', NEDL01) - 1)))
													   ELSE LTRIM(RTRIM(NEDL01))
												  END) Name
										  , CAST(master.dbo.fn_JDETODATE(NEEFTB) AS DATE) StartDate
										  , CAST(SUM(G.NWPMU1 / 100) AS INT) SqFt
										  , ROW_NUMBER() OVER ( PARTITION BY G.NWMCU, G.NWUNIT ORDER BY NEEFTB ) Row_N
								  FROM		MSTR_ODS.dbo.st_F15017 C
										  , MSTR_ODS.dbo.st_F1501B D
										  , MSTR_ODS.dbo.st_F1507 E
										  , MSTR_ODS.dbo.st_F1514 G
								  WHERE		LTRIM(C.NWCO) IN ( SELECT	LTRIM(z.MCCO)
																FROM	MSTR_ODS.dbo.st_F0006 z
																WHERE LTRIM(RTRIM(MCMCU)) IN (SELECT (CASE WHEN LTRIM(RTRIM(B.MCRP41)) IN ('2141', '2142', '2143', '2148', '2351', '2370', '2431', '3008')
																												THEN LTRIM(RTRIM(B.MCMCU)) 
																										ELSE LTRIM(RTRIM(MCMCU)) END )
																							FROM MSTR_ODS.dbo.st_F0006
																							WHERE LTRIM(MCRP08) NOT IN ( '', '300', '400', '600', '990', '995' )
																								AND LTRIM(MCSTYL) IN ( 'PR' )
																								AND MCRP41 <> ''
																								AND MCRP42 = '')
																	AND Z.MCRP41 = B.MCRP41)
											AND C.NWMCU = E.NHMCU
											AND G.NWUNIT = E.NHUNIT
											AND C.NWDOCO = D.NEDOCO
											AND C.NWLSVR = D.NELSVR
											AND C.NWMCU = G.NWMCU
											AND C.NWUNIT = G.NWUNIT
											AND LTRIM(RTRIM(NWARTY)) = CASE NHUTTY
																		 WHEN 'LSABL' THEN 'GLA'
																		 ELSE 'OTH'
																	   END
											AND C.NWLSVR = ( SELECT	MAX(NWLSVR)
															 FROM	MSTR_ODS.dbo.st_F15017 B
															 WHERE	C.NWDOCO = B.NWDOCO
														   )
											AND E.NHUTTY NOT IN ( 'DISAB', 'OTHER', 'OEA', 'UNALL' )
											AND G.NWEFTE = '0'
											AND C.NWMODT = '0'
											AND ( NEEFTE > master.dbo.fn_DATETOJDE(GETDATE())
												  OR NEEFTE = '0'
												)
											AND D.NELSST NOT IN ( 'Z', 'R', 'F' )
											AND D.NELSET <> 'AN'
								  GROUP BY	G.NWMCU
										  , G.NWUNIT
										  , D.NEAN8
										  , D.NEEFTB
										  , D.NELSST
										  , D.NEDL01
										  , E.NHUST
								  UNION
								  SELECT	LTRIM(RTRIM(G.NWUNIT)) Unit
										  , NULL ID
										  , 'AVAILABLE' NAME
										  , NULL StartDate
										  , CAST(SUM(G.NWPMU1 / 100) AS INT) SqFt
										  , 1 Row_N
								  FROM		MSTR_ODS.dbo.st_F1507 E
										  , MSTR_ODS.dbo.st_F1514 G
								  WHERE		LTRIM(E.NHCO) IN ( SELECT	LTRIM(z.MCCO)
																FROM	MSTR_ODS.dbo.st_F0006 z
																WHERE LTRIM(RTRIM(MCMCU)) IN (SELECT (CASE WHEN LTRIM(RTRIM(B.MCRP41)) IN ('2141', '2142', '2143', '2148', '2351', '2370', '2431', '3008')
																												THEN LTRIM(RTRIM(B.MCMCU)) 
																										ELSE LTRIM(RTRIM(MCMCU)) END )
																							FROM MSTR_ODS.dbo.st_F0006
																							WHERE LTRIM(MCRP08) NOT IN ( '', '300', '400', '600', '990', '995' )
																								AND LTRIM(MCSTYL) IN ( 'PR' )
																								AND MCRP41 <> ''
																								AND MCRP42 = '')
																	AND Z.MCRP41 = B.MCRP41)
											AND NWMCU = E.NHMCU
											AND NWUNIT = E.NHUNIT
											AND NWMCU = G.NWMCU
											AND NWUNIT = G.NWUNIT
											AND LTRIM(RTRIM(NWARTY)) = CASE NHUTTY
																		 WHEN 'LSABL' THEN 'GLA'
																		 ELSE 'OTH'
																	   END
											AND E.NHUTTY NOT IN ( 'DISAB', 'OTHER', 'OEA', 'UNALL' )
											AND G.NWEFTE = '0'
											AND E.NHUST = 'V'
								  GROUP BY	NWUNIT
										  , E.NHUST
								) Q
					  WHERE		Q.Row_N = 1
					  ORDER BY	Q.Unit
					FOR
					  XML PATH('Tenant')
						, ROOT('Tenants')
						, TYPE
					)
				  , ( SELECT	CASE CategoryCode
								  WHEN 'MCRP06' THEN 'Region'
								  WHEN 'MCRP16' THEN 'PropertyManager'
								  WHEN 'MCRP17' THEN 'LeasingRep'
								  WHEN 'MCRP21' THEN 'Developer'
								END '@Type'
							  , LTRIM(RTRIM(DRSPHD)) ID
							  , CASE LTRIM(RTRIM(DRSPHD))
								  WHEN '' THEN ''
								  ELSE LTRIM(RTRIM(DRDL01))
								END Name
							  , CASE LTRIM(RTRIM(DRSPHD))
								  WHEN '' THEN ''
								  ELSE LTRIM(RTRIM(DRDL02))
								END Phone
							  , CASE LTRIM(RTRIM(DRSPHD))
								  WHEN '' THEN ''
								  ELSE LTRIM(RTRIM(EAEMAL))
								END Email
					  FROM		( SELECT	pivotA.CategoryCode
										  , pivotA.Value
								  FROM		( SELECT	CAST(LTRIM(Aa.MCRP16) AS VARCHAR(3)) MCRP16
													  , CAST(LTRIM(Aa.MCRP17) AS VARCHAR(3)) MCRP17
													  , CAST(LTRIM(Aa.MCRP21) AS VARCHAR(3)) MCRP21
											  FROM		MSTR_ODS.dbo.st_F0006 Aa
											  WHERE		LTRIM(Aa.MCCO) IN ( SELECT	LTRIM(z.MCCO)
																			 FROM	MSTR_ODS.dbo.st_F0006 z
																			 WHERE LTRIM(RTRIM(MCMCU)) = (CASE WHEN LTRIM(RTRIM(B.MCRP41)) IN ('2141', '2142', '2143', '2148', '2351', '2370', '2431', '3008')
																				THEN LTRIM(RTRIM(B.MCMCU)) ELSE LTRIM(RTRIM(B.MCRP41)) END) )
														AND LTRIM(Aa.MCRP08) NOT IN ( '', '300', '400', '600', '990', '995' )
														AND LTRIM(Aa.MCSTYL) IN ( 'PR' )
											  GROUP BY	Aa.MCRP16
													  , Aa.MCRP17
													  , Aa.MCRP21
											) A UNPIVOT ( Value FOR CategoryCode IN ( MCRP16, MCRP17, MCRP21 ) ) pivotA
								) A
								LEFT JOIN ( SELECT	B.DRKY
												  , B.DRRT
												  , B.DRDL01
												  , B.DRDL02
												  , B.DRSPHD
											FROM	MSTR_ODS.dbo.st_F0005 B
											WHERE	LTRIM(B.DRSY) = '00'
													AND LTRIM(B.DRRT) IN ( '16', '17', '21' )
										  ) Bb
									ON LTRIM(Bb.DRKY) = A.Value
									   AND LTRIM(Bb.DRRT) = RIGHT(A.CategoryCode, 2)
								LEFT JOIN dbo.st_F01151 C
									ON Bb.DRSPHD = C.EAAN8
					FOR
					  XML PATH('Contact')
						, ROOT('Contacts')
						, TYPE
					)
		  FROM		MSTR_ODS.dbo.st_F0006 B
					LEFT JOIN MSTR_ODS.dbo.st_F0005 Status
						ON LTRIM(B.MCRP08) = LTRIM(Status.DRKY)
						   AND LTRIM(Status.DRSY) = '00'
						   AND LTRIM(Status.DRRT) = '08'
					LEFT JOIN MSTR_ODS.dbo.st_F0005 Region
						ON LTRIM(B.MCRP06) = LTRIM(Region.DRKY)
						   AND LTRIM(Region.DRSY) = '00'
						   AND LTRIM(Region.DRRT) = '06'
					LEFT JOIN MSTR_ODS.dbo.st_F0005 MktgGroup
						ON LTRIM(B.MCRP41) = LTRIM(MktgGroup.DRKY)
						   AND LTRIM(MktgGroup.DRSY) = '00'
						   AND LTRIM(MktgGroup.DRRT) = '41'
					LEFT JOIN MSTR_ODS.dbo.st_F0116 J
						ON B.MCAN8 = J.ALAN8
					LEFT OUTER JOIN ( SELECT	SUM(NWPMU1 / 100) SqFt
											  , LEFT(LTRIM(NWMCU), 4) NWMCU
									  FROM		MSTR_ODS.dbo.st_F1514
									  WHERE		NWARTY = 'GLA'
												AND NWARLL = 'F'
												AND NWEFTE = '0'
									  GROUP BY	LEFT(LTRIM(NWMCU), 4)
									) A
						ON B.MCMCU = A.NWMCU
		  WHERE		LTRIM(B.MCRP08) NOT IN ( '', '300', '400', '600', '990', '995' )
					AND LTRIM(B.MCSTYL) IN ( 'PR' )
					AND B.MCRP41 <> ''
					AND B.MCRP42 = ''
					AND b.MCMCU = mgbu.MCMCU
					AND B.MCRP41 = MGBU.MCRP41
					AND B.MCCO = (SELECT MCCO FROM dbo.st_F0006 
								WHERE LTRIM(RTRIM(MCMCU)) = (CASE WHEN LTRIM(RTRIM(B.MCRP41)) IN ('2141', '2142', '2143', '2148', '2351', '2370', '2431', '3008')
																	THEN LTRIM(RTRIM(B.MCMCU)) 
																WHEN LTRIM(RTRIM(B.MCMCU)) IN (SELECT CAST(MIN(CAST(MCMCU AS INT)) AS NVARCHAR(20)) AS MCMCU
																								FROM dbo.st_f0006
																								WHERE LTRIM(RTRIM(MCMCUS)) <> ''
																								AND MCSTYL = 'PR'
																								AND LEN(LTRIM(RTRIM(MCMCU))) > 4
																								AND LTRIM(RTRIM(MCRP41)) <> ''
																								GROUP BY  MCMCUS) THEN LTRIM(RTRIM(dbo.st_f0006.MCMCU))
																ELSE LTRIM(RTRIM(dbo.st_f0006.MCRP41)) END)
									AND MCMCU = B.MCMCU)
									
		  GROUP BY	STATUS.DRDL01
				  , Region.DRDL01
				  , MktgGroup.DRDL01
				  , B.MCRP41
				  , B.MCDL01
				  , B.MCMCU
				  , B.MCDL03
		FOR
		  XML PATH('Property')
			, ROOT('Properties')
			, TYPE
		)
		FROM		MSTR_ODS.dbo.st_F0006 MGBU
					LEFT JOIN MSTR_ODS.dbo.st_F0005 MktgGroup
						ON LTRIM(MGBU.MCRP41) = LTRIM(MktgGroup.DRKY)
						   AND LTRIM(MktgGroup.DRSY) = '00'
						   AND LTRIM(MktgGroup.DRRT) = '41'
		  WHERE		LTRIM(MGBU.MCRP08) NOT IN ( '', '300', '400', '600', '990', '995' )
					AND LTRIM(MGBU.MCSTYL) IN ( 'PR' )
					AND MGBU.MCRP41 <> ''
					AND MGBU.MCRP42 = ''
					AND MGBU.MCCO = (SELECT MCCO FROM dbo.st_F0006 
								WHERE LTRIM(RTRIM(MCMCU)) = (CASE WHEN LTRIM(RTRIM(MGBU.MCRP41)) IN ('2141', '2142', '2143', '2148', '2351', '2370', '2431', '3008')
																	THEN LTRIM(RTRIM(MGBU.MCMCU)) 
																WHEN LTRIM(RTRIM(MGBU.MCMCU)) IN (SELECT CAST(MIN(CAST(MCMCU AS INT)) AS NVARCHAR(20)) AS MCMCU
																									FROM dbo.st_f0006
																									WHERE LTRIM(RTRIM(MCMCUS)) <> ''
																									AND MCSTYL = 'PR'
																									AND LEN(LTRIM(RTRIM(MCMCU))) > 4
																									AND LTRIM(RTRIM(MCRP41)) <> ''
																									GROUP BY  MCMCUS) THEN LTRIM(RTRIM(MGBU.MCMCU))
															ELSE LTRIM(RTRIM(MGBU.MCRP41)) END)
									AND MCMCU = MGBU.MCMCU)
									
		  GROUP BY	MktgGroup.DRDL01
				  , MGBU.MCRP41
				  , MGBU.MCMCU
		FOR
		  XML PATH('Group')
			, ROOT('Groups')
			, TYPE
		)
FOR		XML	PATH('EDENSData')
		  , TYPE        
